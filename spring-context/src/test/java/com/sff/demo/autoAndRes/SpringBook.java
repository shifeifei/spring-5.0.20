package com.sff.demo.autoAndRes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SpringBook {

	@Autowired
	private JavaBook javaBook;

	@PostConstruct
	public void init() {
		System.out.println("@PostConstruct 注解。。。。。");
	}

}
