package com.sff.demo.autoAndRes;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

@Component
public class TestAnnoPostConstruct {

	@Resource
	private SpringBook book;

	@PostConstruct
	public void init() {
		System.out.println("PostConstruct 注解 init 方法");
	}


	@PreDestroy
	public void destroy() {
		System.out.println("SpringBook : " + book);
		System.out.println("PreDestroy 注解 destroy 方法");
	}

}
