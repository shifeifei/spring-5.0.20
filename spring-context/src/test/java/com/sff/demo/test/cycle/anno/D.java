package com.sff.demo.test.cycle.anno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class D {

	@Autowired
	private C c;
}
