package com.sff.demo.test.cycle.anno;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class NotAllowCircularAnnotationConfigApplicationContext extends AnnotationConfigApplicationContext {

	public NotAllowCircularAnnotationConfigApplicationContext(Class<?>... annotatedClasses) {
		super();
		register(annotatedClasses);

		// 禁止循环依赖
		super.setAllowCircularReferences(false);

		refresh();
	}
}
