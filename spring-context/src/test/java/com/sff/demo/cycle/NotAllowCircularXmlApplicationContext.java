package com.sff.demo.cycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NotAllowCircularXmlApplicationContext extends ClassPathXmlApplicationContext {

	public NotAllowCircularXmlApplicationContext(String... configLocations) throws BeansException {
		super(configLocations);
	}

	@Override
	protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory) {
		super.setAllowCircularReferences(true);
		super.customizeBeanFactory(beanFactory);
	}
}
