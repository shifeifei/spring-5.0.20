package com.sff.demo;

import com.sff.demo.autoAndRes.JavaBook;
import com.sff.demo.autoAndRes.SpringBook;
import com.sff.demo.bean.Teacher;
import com.sff.demo.config.BeanConfig;
import com.sff.demo.config.BeanConfig2;
import com.sff.demo.config.BeanConfig3;
import com.sff.demo.cycle.A;
import com.sff.demo.test.cycle.anno.C;
import com.sff.demo.test.cycle.anno.NotAllowCircularAnnotationConfigApplicationContext;
import com.sff.demo.cycle.NotAllowCircularXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAnnoMain {

	public static void main(String[] args) {

		//testComponent();

		//testComponentScan();

		//testPostConstruct();

		// testCycleRef();

		testXmlCycleRef();
	}

	public static void testXmlCycleRef() {
		NotAllowCircularXmlApplicationContext ac = new NotAllowCircularXmlApplicationContext("application-cycle.xml");
		A bean = ac.getBean(A.class);
		System.out.println(bean);
	}

	public static void testCycleRef() {
		NotAllowCircularAnnotationConfigApplicationContext ac = new NotAllowCircularAnnotationConfigApplicationContext(BeanConfig3.class);
		C bean = ac.getBean(C.class);
		System.out.println(bean);
	}

	public static void testPostConstruct() {
		AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(BeanConfig2.class);
		SpringBook bean = ac.getBean(SpringBook.class);
		System.out.println(bean);

		JavaBook javaBook = ac.getBean(JavaBook.class);
		System.out.println(javaBook);

		ac.close();
	}

	public static void testComponentScan() {
		ApplicationContext ac = new AnnotationConfigApplicationContext(BeanConfig.class);
		Teacher user = (Teacher) ac.getBean("teacher");
		System.out.println(user);
	}

	public static void testComponent() {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("application-context.xml");

		Teacher user = (Teacher) cx.getBean("teacher");
		System.out.println(user);
	}

}
