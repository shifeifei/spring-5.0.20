package com.sff.demo.anno;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
@ComponentScan(basePackages = "com.sff.demo.mypackage")
public class TestComponentScanAnno {

	@ComponentScan("com.sff.demo.mypackage")
	@Configuration
	@Order(90)
	class InnerConfig {
	}
}
