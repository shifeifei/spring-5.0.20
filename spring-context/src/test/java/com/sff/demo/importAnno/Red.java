package com.sff.demo.importAnno;

public class Red {
	private String colour;

	public Red() {
		this.colour = "the colour is red !";
	}

	@Override
	public String toString() {
		return "Red{" +
				"colour='" + colour + '\'' +
				'}';
	}
}
