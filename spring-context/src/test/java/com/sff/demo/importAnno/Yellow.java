package com.sff.demo.importAnno;

public class Yellow {

	private String colour;

	public Yellow() {
		this.colour = "the colour is yellow !";
	}

	@Override
	public String toString() {
		return "Red{" +
				"colour='" + colour + '\'' +
				'}';
	}

}
