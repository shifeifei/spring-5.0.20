package com.sff.demo.importAnno;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class CusImportSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		// 返回需要注入的类的全路径类名数组
		return new String[]{"com.sff.demo.importAnno.Blue"};
	}
}
