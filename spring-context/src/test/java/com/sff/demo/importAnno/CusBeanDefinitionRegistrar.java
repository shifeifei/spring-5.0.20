package com.sff.demo.importAnno;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class CusBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

	/**
	 * @param importingClassMetadata 当前类注解信息
	 */
	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

		RootBeanDefinition definition = new RootBeanDefinition(Yellow.class);
		registry.registerBeanDefinition("yellow", definition);

	}
}
