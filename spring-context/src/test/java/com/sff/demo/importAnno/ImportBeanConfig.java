package com.sff.demo.importAnno;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({Red.class, CusImportSelector.class, CusBeanDefinitionRegistrar.class})
public class ImportBeanConfig {
}
