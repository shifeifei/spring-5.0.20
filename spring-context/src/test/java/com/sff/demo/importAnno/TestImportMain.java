package com.sff.demo.importAnno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestImportMain {

	public static void main(String[] args) {

		ApplicationContext ac = new AnnotationConfigApplicationContext(ImportBeanConfig.class);
		System.out.println(ac.getBean(Red.class));

		System.out.println(ac.getBean(Blue.class));

		System.out.println(ac.getBean(Yellow.class));
	}

}
