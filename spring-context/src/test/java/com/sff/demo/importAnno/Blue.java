package com.sff.demo.importAnno;

public class Blue {

	private String colour;

	public Blue() {
		this.colour = "the colour is blue !";
	}

	@Override
	public String toString() {
		return "Blue{" +
				"colour='" + colour + '\'' +
				'}';
	}
}
