package com.sff.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;

public class LogXmlAspect {

	public void before() {
		System.out.println("!!! LogXmlAspect , before ...");
	}

	public void after() {
		System.out.println("!!! LogXmlAspect , after ...");
	}

	public void afterReturning(String result) {
		System.out.println("!!! LogXmlAspect , after returning , " + result);
	}

	public void afterThrowing(Exception e) {
		System.out.println("!!! LogXmlAspect , after throwing , " + e.getMessage());
	}

	public void around(ProceedingJoinPoint pjp) {
		Signature signature = pjp.getSignature();
		Object result = null;

		try {
			System.out.println("!!! LogXmlAspect Around start , the method is " + signature.getName());
			// 通过反射方式调用目标方法
			result = pjp.proceed(pjp.getArgs());
			System.out.println("!!! LogXmlAspect Around end , the method is " + signature.getName() + ", res ：" + result);
		} catch (Throwable e) {
			System.out.println("!!! LogXmlAspect Around exception , the method is " + signature.getName() + " , " + e.getMessage());
		}
	}
}
