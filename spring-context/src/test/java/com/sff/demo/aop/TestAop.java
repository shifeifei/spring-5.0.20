package com.sff.demo.aop;

import com.sff.demo.config.AopConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAop {

	public static void main(String[] args) {

		testAopXml();

		// testAopAnno();
	}

	private static void testAopXml() {
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("aop.xml");
		PrintMsgService aop = ac.getBean(PrintMsgService.class);
		aop.printMsg();
	}

	private static void testAopAnno() {
		AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(AopConfig.class);
		PrintMsgService aop = ac.getBean(PrintMsgService.class);
		aop.printMsg();
	}


}
