package com.sff.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAnnoAspect {

	// 定义切点
	@Pointcut("execution(* com.sff.demo.aop.PrintMsgService.*(..))")
	public void pointCut() {
	}

	@Before(value = "pointCut()")
	private void before(JoinPoint joinPoint) {
		// 获取方法签名
		Signature signature = joinPoint.getSignature();
		System.out.println(">>>> LogAnnoAspect Before , Test Aop , the method is " + signature.getName());
	}

	@After("pointCut()")
	public void after(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		System.out.println(">>>> LogAnnoAspect After , Test Aop , the method is " + signature.getName());
	}

	@AfterReturning("pointCut()")
	public void afterReturning(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		System.out.println(">>>> LogAnnoAspect AfterReturning , Test Aop , the method is " + signature.getName());
	}

	@AfterThrowing("pointCut()")
	public void afterThrowing(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		System.out.println(">>>> LogAnnoAspect AfterThrowing , Test Aop , the method is " + signature.getName());
	}

	@Around("pointCut()")
	public Object around(ProceedingJoinPoint pjp) {

		Signature signature = pjp.getSignature();
		Object result = null;

		try {
			System.out.println(">>>> LogAnnoAspect Around start, Test Aop , the method is " + signature.getName());
			// 通过反射方式调用目标方法
			result = pjp.proceed(pjp.getArgs());
			System.out.println(">>>> LogAnnoAspect Around end, Test Aop , the method is " + signature.getName());
		} catch (Throwable e) {
			System.out.println(">>>> LogAnnoAspect Around exception, Test Aop , the method is " + signature.getName() + " , " + e.getMessage());
		}

		return result;
	}

}
