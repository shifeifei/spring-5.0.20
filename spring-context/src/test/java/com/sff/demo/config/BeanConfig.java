package com.sff.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan(basePackages = "com.sff.demo")
@ImportResource(locations = "classpath:application-context.xml")
public class BeanConfig {
}
