package com.sff.demo.config;

import com.sff.demo.autoAndRes.JavaBook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "com.sff.demo.test.cycle.anno")
public class BeanConfig3 {
}
