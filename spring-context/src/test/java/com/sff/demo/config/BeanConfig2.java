package com.sff.demo.config;

import com.sff.demo.autoAndRes.JavaBook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "com.sff.demo.autoAndRes")
public class BeanConfig2 {

	@Bean(name = "javaBook22")
	public JavaBook book() {
		System.out.println("调用了 @Bean 注解的方法 ... ");
		return new JavaBook();
	}

}
