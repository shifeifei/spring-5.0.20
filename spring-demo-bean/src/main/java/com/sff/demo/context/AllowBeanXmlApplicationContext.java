package com.sff.demo.context;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AllowBeanXmlApplicationContext extends ClassPathXmlApplicationContext {

	public AllowBeanXmlApplicationContext(String... configLocations) throws BeansException {
		super(configLocations);
	}

	@Override
	protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory) {

		// 允许覆盖同名称的不同定义的对象
		super.setAllowBeanDefinitionOverriding(true);
		// 允许bean之间的循环依赖
		super.setAllowCircularReferences(true);

		super.customizeBeanFactory(beanFactory);
	}
}
