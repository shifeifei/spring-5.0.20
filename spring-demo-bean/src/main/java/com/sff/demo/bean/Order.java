package com.sff.demo.bean;


public class Order {
	private String orderNo;
	private String address;

	public Order(String orderNo, String address) {
		this.orderNo = orderNo;
		this.address = address;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
