package com.sff.demo.bean;

import org.springframework.beans.factory.FactoryBean;

public class CarBeanFactory implements FactoryBean<Car> {

	@Override
	public Car getObject() throws Exception {
		Car car = new Car();
		car.setCarName("奔驰");
		return car;
	}

	@Override
	public Class<?> getObjectType() {
		return Car.class;
	}
}
