package com.sff.demo.bean;

public class Company {
	private String id;
	private String name;

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String get() {
		return id + "," + name;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
