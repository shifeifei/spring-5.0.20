package com.sff.demo.bean;

public class Bus {
	private String busNo;
	private Company company;

	public void setBusNo(String busNo) {
		this.busNo = busNo;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getBusNo() {
		return busNo;
	}

	public Company getCompany() {
		return company;
	}
}
