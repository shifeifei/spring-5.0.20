package com.sff.demo.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.Ordered;

public class NewBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, Ordered {

	private String processor;

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println(" **** 执行了：NewBeanDefinitionRegistryPostProcessor 的 postProcessBeanFactory 方法 **** ");
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		System.out.println(" **** 执行了：NewBeanDefinitionRegistryPostProcessor 的 postProcessBeanDefinitionRegistry 方法 **** ");
	}

	@Override
	public int getOrder() {
		return 0;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	@Override
	public String toString() {
		return "NewBeanDefinitionRegistryPostProcessor { " +
				"processor = '" + processor + '\'' +
				'}';
	}
}
