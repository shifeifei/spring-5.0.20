package com.sff.demo.processor;

import com.sff.demo.bean.User;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class UserBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof User) {
			User u = (User) bean;
			System.out.println("postProcessBeforeInitialization beanName : " + beanName + ",bean : " + u.getUsername());
			((User) bean).setUsername("AAAA");
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof User) {
			User u = (User) bean;
			System.out.println("postProcessAfterInitialization beanName : " + beanName + ",bean : " + u.getUsername());
			((User) bean).setUsername("BBB");
		}
		return bean;
	}
}
