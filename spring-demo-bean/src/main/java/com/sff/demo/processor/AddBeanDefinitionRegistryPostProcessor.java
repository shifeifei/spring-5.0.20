package com.sff.demo.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.Ordered;


public class AddBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, Ordered {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("《《《《执行了：AddBeanDefinitionRegistryPostProcessor 的 postProcessBeanFactory 方法》》》》");
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		System.out.println("《《《《执行了：AddBeanDefinitionRegistryPostProcessor 的 postProcessBeanDefinitionRegistry 方法，并新增一个 BeanDefinitionRegistryPostProcessor  》》》》");
		BeanDefinitionBuilder newDef = BeanDefinitionBuilder.rootBeanDefinition(NewBeanDefinitionRegistryPostProcessor.class);
		newDef.addPropertyValue("processor", "NewBeanDefinitionRegistryPostProcessor");
		registry.registerBeanDefinition("newBeanDef", newDef.getBeanDefinition());
	}

	@Override
	public int getOrder() {
		return 0;
	}
}
