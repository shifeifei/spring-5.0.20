package com.sff.demo.custom.tag;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

// 命名空间解析器
public class UserLoginNamespaceHandler extends NamespaceHandlerSupport {

	@Override
	public void init() {
		// 这里注册的 elementName 就是你的 xsd 文件中 <element name="user"/> 的name属性值
		registerBeanDefinitionParser("user", new UserLoginBeanDefinitionParser());
	}

	// 自定义标签属性解析器
	private static class UserLoginBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

		@Override
		protected Class<?> getBeanClass(Element element) {
			return UserLogin.class;
		}

		@Override
		protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {

			String uId = element.getAttribute("id");
			if (StringUtils.hasLength(uId)) {
				builder.addPropertyValue("id", uId);
			}

			String uName = element.getAttribute("u-name");
			if (StringUtils.hasLength(uName)) {
				builder.addPropertyValue("username", uName);
			}

			String uPassword = element.getAttribute("u-password");
			if (StringUtils.hasLength(uPassword)) {
				builder.addPropertyValue("password", uPassword);
			}
		}
	}
}
