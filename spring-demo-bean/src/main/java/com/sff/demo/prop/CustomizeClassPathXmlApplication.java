package com.sff.demo.prop;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CustomizeClassPathXmlApplication extends ClassPathXmlApplicationContext {

	public CustomizeClassPathXmlApplication(String configLocation) {
		super(configLocation);
	}

	@Override
	protected void initPropertySources() {
		// 这里定制自己的业务逻辑
		System.out.println("扩展 initPropertySources");

		// 设置容器启动必要参数
		getEnvironment().setRequiredProperties("docker_env");
	}
}
