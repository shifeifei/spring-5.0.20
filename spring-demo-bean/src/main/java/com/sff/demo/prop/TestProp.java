package com.sff.demo.prop;

import com.sff.demo.bean.User;
import org.springframework.context.ApplicationContext;

public class TestProp {

	public static void main(String[] args) {
		testInitPropertySources();
	}

	public static void testInitPropertySources() {
		ApplicationContext cx = new CustomizeClassPathXmlApplication("application-bean.xml");
		User user = cx.getBean("user", User.class);
		System.out.println(user.getUsername());
	}
}
