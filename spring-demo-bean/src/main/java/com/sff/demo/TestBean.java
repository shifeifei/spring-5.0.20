package com.sff.demo;

import com.sff.demo.bean.*;
import com.sff.demo.createBean.bean.Book;
import com.sff.demo.createBean.bean.House;
import com.sff.demo.custom.tag.UserLogin;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Arrays;

public class TestBean {

	/**
	 * GenericApplicationContext 和 ClassPathXmlApplicationContext 的区别
	 */
	public static void main(String[] args) {
		//getUserNoIdAndName();
		//testCustomTag();
		//testSpEL();
		//testPropertyEditor();
		//testBeanPostProcessor();
		//getUserNoIdAndName();

		 //createBeanProcessor();

		//createBeanSupplier();

		createBeanBook();

	}

	public static void createBeanBook() {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("create-bean-book.xml");
		Book house =  cx.getBean(Book.class);
		System.out.println(house.getName());
	}

	public static void createBeanSupplier() {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("create-bean-supplier.xml");
		House house = (House) cx.getBean("house");
		System.out.println(house.getAddress());
	}

	public static void createBeanProcessor() {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("create-bean-processor.xml");
		House house = (House) cx.getBean("house");
		System.out.println(house.getAddress());
	}


	public static void testBeanPostProcessor() {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("application-bfpp.xml");
		System.out.println("testBeanPostProcessor 运行结果 : " + cx.getBean("newBeanDef"));
	}


	public static void testPropertyEditor() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-property-editor.xml");
		User user = (User) cx.getBean("user5");
		System.out.println(user.getUsername() + "; " + user.getBirthday());
	}

	public static void testSpEL() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-dev-bean.xml");
		User user = (User) cx.getBean("user3");
		System.out.println(user.getUsername());
	}

	public static void testCustomTag() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-custom-bean.xml");
		UserLogin user = (UserLogin) cx.getBean("A1106");
		System.out.println(user);
	}


	public static void getUserNoIdAndName() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
//		User user1 = cx.getBean("com.sff.demo.bean.User#0", User.class);
//		System.out.println("用户名111：" + user1.getUsername());
//
		User user = (User) cx.getBean("user");
		System.out.println("用户名：" + user.getUsername());
	}


	public static void testCompany() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
		Company c = cx.getBean("company", Company.class);
		System.out.println(c.get());
	}


	public static void testBus() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
		Bus bus = cx.getBean("bus", Bus.class);
		System.out.println(bus.getCompany().get() + "," + bus.getBusNo());
	}


	public static void testCollection() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
		Collection col = cx.getBean("collection", Collection.class);
		System.out.println(Arrays.toString(col.getArrays()));
		System.out.println(col.getLists());
		System.out.println(col.getSets());
		System.out.println(col.getMaps());
	}


	public static void factoryBean() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
		Car car = cx.getBean("car", Car.class);
		Car carId = (Car) cx.getBean("carId");
		System.out.println(carId.getCarName());
		System.out.println(car.getCarName());
	}


	public static void defaultContext() {
		ApplicationContext cx = new ClassPathXmlApplicationContext("application-bean.xml");
		Car car = cx.getBean("bwmCar", Car.class);
		System.out.println(car.getCarName());
	}


	public static void defaultFactory() {
		DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
		Resource resource = new ClassPathResource("application-bean.xml");
		reader.loadBeanDefinitions(resource);

		System.out.println(factory.getBean("bwmCar", Car.class).getCarName());
	}


	public static void defaultSingleton() {

		DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);

		Resource resource = new ClassPathResource("application-bean.xml");

		reader.loadBeanDefinitions(resource);
	}

}
