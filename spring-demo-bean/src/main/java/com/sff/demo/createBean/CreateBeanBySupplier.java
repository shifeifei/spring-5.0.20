package com.sff.demo.createBean;

import com.sff.demo.createBean.bean.House;

public class CreateBeanBySupplier {
	public static House getBean() {
		House house = new House();
		house.setAddress("别墅");
		return house;
	}
}
