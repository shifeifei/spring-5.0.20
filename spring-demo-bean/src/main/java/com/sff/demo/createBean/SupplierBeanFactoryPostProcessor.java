package com.sff.demo.createBean;

import com.sff.demo.createBean.bean.House;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;

public class SupplierBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

		BeanDefinition beanDefinition = beanFactory.getBeanDefinition("house");
		GenericBeanDefinition house = (GenericBeanDefinition) beanDefinition;
		house.setInstanceSupplier(CreateBeanBySupplier::getBean);
		//house.setBeanClass(House.class);

	}
}
