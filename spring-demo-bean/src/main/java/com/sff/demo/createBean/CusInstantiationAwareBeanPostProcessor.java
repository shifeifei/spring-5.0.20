package com.sff.demo.createBean;

import com.sff.demo.bean.Student;
import com.sff.demo.createBean.bean.House;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

public class CusInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

	// BeanPostProcessor 接口的方法
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("CCCC--执行了 BeanPostProcessor 的 postProcessBeforeInitialization 方法，说明属性填充已完成");
		return bean;
	}

	// BeanPostProcessor 接口的方法，该方法是在 populateBean 填充属性方法中调用的
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("DDDD--执行了 BeanPostProcessor 的 postProcessAfterInitialization 方法，说明属性填充已完成");
		if (bean instanceof House) {
			House h = (House) bean;
			h.setAddress("乡村");
			bean = h;
		}
		return bean;
	}

	/**
	 * 在bean实例化之后调用，即创建bean对象完成后调用
	 */
	@Override
	public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
		System.out.println("AAAA--执行了 InstantiationAwareBeanPostProcessor 的 postProcessBeforeInstantiation 方法");
		if (!beanClass.equals(House.class)) {
			return null;
		}
		// 在该方法中实现 cglib 代理的方式实现完成对象的创建，此时 Spring 容器在实例化 bean 时
		Enhancer en = new Enhancer();
		en.setSuperclass(beanClass);
		// 设置回调函数
		en.setCallback(new MyMethodInterceptor());
		// 返回代理对象
		House house = (House) en.create();
		return house;
	}

	private class MyMethodInterceptor implements MethodInterceptor {
		/**
		 * @param o           表示增强的对象，即实现这个接口类的一个对象
		 * @param method      表示要拦截的方法
		 * @param objects     表示被拦截的方法的参数
		 * @param methodProxy 表示要触发父类的方法对象
		 * @return
		 */
		@Override
		public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
			// cglib 代理调用 invoke 和 invokeSuper 方法的实现方式是不同的
			Object returnValue = methodProxy.invokeSuper(o, objects);
			return returnValue;
		}
	}

	/**
	 * 在bean实例化之后调用，填充属性值之前调用
	 * (1) 该方法返回 true 时会执行 bean 对象属性填充逻辑，如果返回 false ，不执行属性填充逻辑逻辑
	 */
	@Override
	public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
		System.out.println("BBBB--执行了 InstantiationAwareBeanPostProcessor 的 postProcessAfterInstantiation 方法");
		return true;
	}

	@Override
	public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeansException {
		return pvs;
	}
}
