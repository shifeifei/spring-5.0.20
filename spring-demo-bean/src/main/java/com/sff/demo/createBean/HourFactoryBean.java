package com.sff.demo.createBean;

import com.sff.demo.createBean.bean.House;
import org.springframework.beans.factory.FactoryBean;

public class HourFactoryBean implements FactoryBean<House> {

	@Override
	public House getObject() throws Exception {
		House house = new House();
		house.setAddress("西安");
		return house;
	}

	@Override
	public Class<?> getObjectType() {
		return House.class;
	}
}
