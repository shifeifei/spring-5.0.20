package com.sff.demo.createBean.bean;

public class MulConstructor {

	private String id;
	private String name;
	private Integer age;

	public MulConstructor() {
	}

	public MulConstructor(String id) {
		this.id = id;
	}

	public MulConstructor(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public MulConstructor(String id, String name) {
		this.name = name;
		this.id = id;
	}

	public MulConstructor(String id, String name, Integer age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
